
package sumtime;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.jdom2.Attribute;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.XMLOutputter;

/**
 *
 * @author sumTime Group
 */
public class Analise {
   
    /**
     * Método responsável pela geração do arquivo XML
     * 
    */
    public static void write() throws FileNotFoundException, IOException{
       String activ[] = {"Semana Academica", "FISL", "EMICRO", "Bolsa IC", "CLEI"};

       Document doc = new Document();

       Element root = new Element("lista");
       Attribute curso = new Attribute("curso","Ciencia da Computação");
       root.setAttribute(curso);
       
       for(int i=0; i<activ.length; i++){
           Element atividade = new Element("atividade");

           Element nome = new Element("nome");
           nome.setText(activ[i]);
           atividade.addContent(nome);
           
           Element tipo = new Element("tipo");
           tipo.setText("Evento Internacional");
           atividade.addContent(tipo);

           Element modalidade = new Element("modalidade");
           modalidade.setText("ensino");
           atividade.addContent(modalidade);

           Element horas = new Element("horas");
           horas.setText("20");
           atividade.addContent(horas);

           root.addContent(atividade);
       }

       doc.setRootElement(root);

       XMLOutputter outputter = new XMLOutputter();
       outputter.output(doc, new FileOutputStream(new File("hours.xml")));
    }
    

    /**
     * Método responsavel pela leitura do arquivo XML do aluno
     */
    public static void read() throws JDOMException, IOException{
        SAXBuilder reader = new SAXBuilder();
        Document doc = reader.build(new File("hours.xml"));
        
        List<Attribute> curso = doc.getRootElement().getAttributes();
        String s = curso.toString();
        Pattern p = Pattern.compile("\"(.*?)\"");
        Matcher m = p.matcher(s); 
        if (m.find()) {
            String cursoNome = m.group(1);
        } 
        
        Element lista = doc.getRootElement();
        List<Element> atividades = lista.getChildren();
        
        ArrayList<String> act = new ArrayList<String>();
        for(Element e: atividades){
             act.add(e.getChildText("nome"));
             act.add(e.getChildText("tipo"));
             act.add(e.getChildText("modalidade"));
             act.add(e.getChildText("horas"));
        }
        
    }
    
    /**
     * Método responsável pela leitura do arquivo XML default (informado pelo prof)
     */
    public static void readDefault() throws JDOMException, IOException{
        SAXBuilder reader = new SAXBuilder();
        Document doc = reader.build(new File("default.xml"));
        
        List<Attribute> curso = doc.getRootElement().getAttributes();
        String s = curso.toString();
        Pattern p = Pattern.compile("\"(.*?)\"");
        Matcher m = p.matcher(s); 
        if (m.find()) {
            String cursoNome = m.group(1);
        } 
        
        Element lista = doc.getRootElement();
        List<Element> atividades = lista.getChildren();
        
        ArrayList<String> actDefault = new ArrayList<String>();
        for(Element e: atividades){
             actDefault.add(e.getChildText("nome"));
             actDefault.add(e.getChildText("categoria")); 
             actDefault.add(e.getChildText("hora"));
             actDefault.add(e.getChildText("horaMax"));
        }
    }
}

