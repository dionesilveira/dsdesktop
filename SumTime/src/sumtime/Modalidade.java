/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package sumtime;

/**
 *
 * @author angelin
 */
public class Modalidade {

    private String categoria;
    private float minimoHoras;
    
    //Método Construtor. Primeiro sem nada e segundo com parâmetros
    private Modalidade(){
        this.categoria = "";
        this.minimoHoras = 0;
    }
    
    private Modalidade(String categoria, float minimoHoras){
        this.categoria = categoria;
        this.minimoHoras = minimoHoras;
    }
    
    //Método Set Categoria
    public void setCategoria(String categoria){
        this.categoria = categoria;
    }
    
    //Método Get Categoria
    public String getCategoria(){
        return categoria;
    }
    
    //Método Set Mínimo Horas
    public void setMinimoHoras(float minimoHoras){
        this.minimoHoras = minimoHoras;
    }
    
    //Método Get Mínimo Horas
    public float getMinimoHoras(){
        return minimoHoras;
    }
}
