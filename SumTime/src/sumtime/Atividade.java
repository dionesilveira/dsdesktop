/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package sumtime;

/**
 *
 * @author Dione
 */
class Atividade {
    private Modalidade modalidade;
    private float horas;
    private TipoAtividade tipoAtividade;
    private String nome;
    private float maximoHoras;
    
    
    public Atividade(String nomeAtividade, float horas ){
        
        this.nome = nomeAtividade;
        this.horas = horas;
        this.tipoAtividade = tipoAtividade.getAtividadeSugerida(nomeAtividade);
    }
    
    public Atividade(String nomeAtividade ){
        this.nome = nomeAtividade;
        this.tipoAtividade = tipoAtividade.getAtividadeSugerida(nomeAtividade);
        
    }
    
    public void setModalidade(Modalidade modalidade) {
        this.modalidade.setMinimoHoras(modalidade.getMinimoHoras());
        this.modalidade.setCategoria(modalidade.getCategoria());
    }

    public void setHoras(float horas) {
        this.horas = horas;
    }

    public void setTipoAtividade(TipoAtividade tipoAtividade) {
        this.tipoAtividade.setNome(tipoAtividade.getNome());
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Modalidade getModalidade() {
        return modalidade;
    }

    public float getHoras() {
        return horas;
    }

    public TipoAtividade getTipoAtividade() {
        return tipoAtividade;
    }

    public String getNome() {
        return nome;
    }

    public float getMaximoHoras() {
        return maximoHoras;
    }

    
}
