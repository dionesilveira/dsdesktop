/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package sumtime;

/**
 *
 * @author Dione
 */
class Curso {
    
    private String nome;
    private float horasNecessarias;
    
    public Curso(String nome, float horasNecessarias){
        this.nome  = nome;
        this.horasNecessarias = horasNecessarias;
    }

    public String getNome() {
        return nome;
    }

    public float getHorasNecessarias() {
        return horasNecessarias;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public void setHorasNecessarias(float horasNecessarias) {
        this.horasNecessarias = horasNecessarias;
    }
    
    
    
}